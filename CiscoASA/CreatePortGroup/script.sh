#!/bin/bash
clear
echo '***********************************************'
echo '**                                           **'
echo '**             Script Cisco ASA              **'
echo '**    Criar grupo com Portas como objetos    **'
echo '**                                           **'
echo '***********************************************'
echo 
echo Pressione uma tecla para continuar
read x
    clear
    objects_tcp=$(cat tcp.txt | wc -l)
    objects_udp=$(cat udp.txt | wc -l)
    tcp=(${tcp[@]} `cat tcp.txt`)
    udp=(${udp[@]} `cat udp.txt`)
    echo "Insira o nome do grupo de portas novo:"
    read group
    clear
    echo
        echo '#'
        echo conf t
        echo '#'
        for (( c=0; c<=$objects_tcp; c++ )) do
            echo object service ${tcp[$c]}
            echo service tcp destination eq ${tcp[$c]}
        done
         for (( c=0; c<=$objects_udp; c++ )) do
            echo object service ${udp[$c]}
            echo service udp destination eq ${udp[$c]}
        done       
        echo object-group service $group
        for (( c=0; c<=$objects_tcp; c++ )) do
            echo service-object object ${tcp[$c]}
        done
        for (( c=0; c<=$objects_udp; c++ )) do
            echo service-object object ${udp[$c]}
        done
        echo exit
        echo wr
        echo '#'
        echo '#'
        